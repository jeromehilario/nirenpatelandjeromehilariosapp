package com.example.jeromehilario.nirenpatelandjeromehilariosapp;

// It was important to import the 'Uri' resource from the android net library for the correct functioning of the button.
import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

// The public class access modifier allows other objects to access and call it
public class CentralLandmark extends AppCompatActivity {

    // The protected access modifier allows all subclasses and the package to access it
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_central_landmark);

        addbuttonClickListener();
        }
    // The private access modifier prevents confusion between other declared buttons that may have the same variable name.
    private void addbuttonClickListener() {
    Button launchBB = (Button) findViewById(R.id.launchBB);
    launchBB.setOnClickListener(new View.OnClickListener() {


       @Override
        public void onClick(View v) {

// A Uri object has been created.
// With the click of the button, the url, and hence the location of the landmark on Google Maps is opened.
            Intent intentBB = new Intent(Intent.ACTION_VIEW,Uri.parse("https://www.google.co.uk/maps/place/Big+Ben/@51.5007292,-0.1268141,17z/data=!3m1!4b1!4m2!3m1!1s0x487604c38c8cd1d9:0xb78f2474b9a45aa9"));
        startActivity(intentBB);
        }

    });


    }
}

