package com.example.jeromehilario.nirenpatelandjeromehilariosapp;

// These are import declarations that refer to data and methods in other packages
// Importing AdapterView, ArrayAdapter and ListView from the Android widget library was very important for the proper functioning of the activity.
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

// The public class access modifier allows other objects to access and call it
public class Central extends AppCompatActivity{

    @Override

// The protected access modifier allows all subclasses and the package to access it
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_central);

        // Titles for the ListView are contained in array as strings.
        String[] subList = {"Random Facts", "General Information", "Landmarks"};

        // The ArrayAdapter class handles the array objects, in this case, strings to bind them to the TextView.
        // Once the adapter has been set; they can be combined with the onClickListener interface to function.
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, subList);
        ListView list1 = (ListView) findViewById(R.id.centralList);
        list1.setAdapter(adapter);

// Switch statements have been used along with the OnItemClickListener interface.
// When a desired object is clicked in the listView, then it will navigate to that page based on what is pressed.
        list1.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
// Although switch statements have been used in this case; if and else statements could also have been used.
// The word position enclosed in the switch statement, refers to the position of the statement in the array and hence the list.

                switch(position){
                    case 0:
                        Intent goR = new Intent(Central.this, CentralRandom.class);
                        startActivity(goR);
                        break;

                    case 1:
                        Intent goGI = new Intent(Central.this, CentralGI.class);
                        startActivity(goGI);
                        break;
                    case 2:
                        Intent goLandmark = new Intent(Central.this, CentralLandmark.class);
                        startActivity(goLandmark);
                        break;
                }

            }
        })
        ;}
}