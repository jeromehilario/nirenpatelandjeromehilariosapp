package com.example.jeromehilario.nirenpatelandjeromehilariosapp;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

// The public class access modifier allows other objects to access and call it
public class SouthRandom extends AppCompatActivity {

// A protected access modifier is used so that when facts are referred to, there is no confusion with other activities under the random name.
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_south_random);

// the final statement is used alongside TextView so that text is displayed in a TextView that currently has no content.
        final TextView textView = (TextView) findViewById(R.id.TextRS);

// declaring the button widget with this piece of code:
        Button press = (Button) findViewById(R.id.pressRS);

// All facts are stored in strings enclosed by quotation marks and separated by commas.
        final String [] facts = {"St. Thomas’ Hospital in Lambeth is an NHS teaching hospital that was established in the late 12th. It is believed that, at one point, the hospital had seven buildings, with one for each day of the week. This was to help doctors and staff know when the patient had been admitted to the hospital, as patients were assigned to a building based on the date of admittance. Only two of those buildings still remain.",
                "The Croydon Airport was one of the first airports to offer commercial flights and was the first airport in the world to have air traffic control. The world’s first public railway was also in Croydon; it opened in 1803 and connected the borough to Wandsworth. This was instrumental in beginning the era of commuting—one that current London residents probably dread.",
                "Battersea Power Station in Wandsworth has long been a London icon, even though it is no longer used to supply power to the city. Pink Floyd has been linked to the power station through its albums, especially Animals, the cover of which featured an inflatable pig flying above the station. It’s said that, on the day of shooting, the pig broke free of its moorings and actually caused a disruption in flight paths of Heathrow Airport.",
                "The first weather report in the world was issued from the Greenwich Royal Observatory by James Glaisher in 1848 and was published in the London Daily News. Glaisher was a founding member of the British Meteorological Society in 1850.",
                "The O2 Arena in Greenwich, originally known as the Millennium Dome, is so large that it can fit the Great Pyramid of Giza or the Statue of Liberty inside."};

// The Math.Random function is used to generate a random integer.
// setText interface us used to tell Java what to display.
// 'facts' is specified in the brackets to refer to that array.
// [random] is the integer generated by pressing the button and also refers to the position in the array.
// Although positions range from 0 to n in the array (inclusive); Math.Random generates integers which are decimals rounded down.
// Hence numbers generated by the Math.Random function are actually from 0 to n in this case, completely corresponding to the array positions.
// More facts can be added, but that would also require an increase in the number below by the same increment.
        press.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int random = (int) ((Math.random() * 5));
                textView.setText(facts[random]);
            }
        });
    }
}