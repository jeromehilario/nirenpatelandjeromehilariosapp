package com.example.jeromehilario.nirenpatelandjeromehilariosapp;

// It was important to import the 'Uri' resource from the android net library for the correct functioning of the button.
import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;


// The public class access modifier allows other objects to access and call it
public class EastLandmark extends AppCompatActivity {

    // The protected access modifier allows all subclasses and the package to access it
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_east_landmark);

        addbuttonClickListener();
    }
    // The private access modifier prevents confusion between other declared buttons that may have the same variable name.
    private void addbuttonClickListener() {
        Button launchOP = (Button) findViewById(R.id.launchOP);
        launchOP.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

// A Uri object has been created.
// With the click of the button, the url, and hence the location of the landmark on Google Maps is opened.

                Intent intentOP = new Intent(Intent.ACTION_VIEW, Uri.parse("https://www.google.co.uk/maps/place/Queen+Elizabeth+Olympic+Park/@51.5432698,-0.0146661,17z/data=!3m1!4b1!4m2!3m1!1s0x48761d6f0c0dfe91:0x9f8a1be15e0d425c"));
                startActivity(intentOP);
            }

        });


    }
}

