package com.example.jeromehilario.nirenpatelandjeromehilariosapp;

// It was important to import the 'Uri' resource from the android net library for the correct functioning of the button.
import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

// The public class access modifier allows other objects to access and call it
public class WestLandmark extends AppCompatActivity {

    // The protected access modifier allows all subclasses and the package to access it
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_west_landmark);
        addbuttonClickListener();
    }

    // The private access modifier prevents confusion between other declared buttons that may have the same variable name.
    private void addbuttonClickListener() {
        Button launchRA = (Button) findViewById(R.id.launchRA);
        launchRA.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

// A Uri object has been created.
// With the click of the button, the url, and hence the location of the landmark on Google Maps is opened.
                Intent intentRA = new Intent(Intent.ACTION_VIEW, Uri.parse("https://www.google.co.uk/maps/place/Royal+Albert+Hall/@51.5009105,-0.1784603,18z/data=!3m1!5s0x4876055b3cf49b6b:0xb02b18b508266975!4m6!1m3!3m2!1s0x4876055b21867ad1:0x5efe9cee35da2fd9!2sRoyal+Albert+Hall!3m1!1s0x4876055b21867ad1:0x5efe9cee35da2fd9"));
                startActivity(intentRA);
            }

        });
    }}