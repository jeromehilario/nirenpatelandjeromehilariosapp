package com.example.jeromehilario.nirenpatelandjeromehilariosapp;


// There was not much coding required for the General information activities since .xml layout files was the only requirement.
// There was also no requirement of user interaction on these pages.
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;


public class CentralGI extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_central_gi);
    }
}
