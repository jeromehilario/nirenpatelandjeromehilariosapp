package com.example.jeromehilario.nirenpatelandjeromehilariosapp;

// These are import declarations that refer to data and methods in other packages
// I had to import the the android widget, button so that they could be used correctly.
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

// The public class access modifier allows other objects to access and call it
public class London extends AppCompatActivity implements View.OnClickListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_london);

//   Button objects are declared here in a protected access modifier so there is no confusion with other activities that use buttons.
        Button button = (Button) findViewById(R.id.button);
        Button button2 = (Button) findViewById(R.id.button2);
        Button button3 = (Button) findViewById(R.id.button3);
        Button button4 = (Button) findViewById(R.id.button4);
        Button button5 = (Button) findViewById(R.id.button5);

// Buttons are given a setOnClickListener interface so that they can be interacted with
// 'this' is used to reference the setOnClickListener to each button.
// This could have also been done with an array.
        button.setOnClickListener(this);
        button2.setOnClickListener(this);
        button3.setOnClickListener(this);
        button4.setOnClickListener(this);
        button5.setOnClickListener(this);
    }

// Public access modifier is used once again as the variables will be unique and there will be no clash
// Switch statements are used with Intent, so that a new activity is opened based on what the user decides to press.
    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.button:
                Intent gocentral = new Intent(London.this, Central.class);
                startActivity(gocentral);
                break;

            case R.id.button2:
                Intent gowest = new Intent(London.this, West.class);
                startActivity(gowest);
                break;

            case R.id.button3:
                Intent gonorth = new Intent(London.this, North.class);
                startActivity(gonorth);
                break;

            case R.id.button4:
                Intent goeast = new Intent(London.this, East.class);
                startActivity(goeast);
                break;

            case R.id.button5:
                Intent gosouth = new Intent(London.this, South.class);
                startActivity(gosouth);
                break;
        }
    }
}