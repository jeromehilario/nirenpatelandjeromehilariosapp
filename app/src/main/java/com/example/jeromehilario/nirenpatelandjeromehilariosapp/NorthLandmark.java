package com.example.jeromehilario.nirenpatelandjeromehilariosapp;

// It was important to import the 'Uri' resource from the android net library for the correct functioning of the button.
import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

// The public class access modifier allows other objects to access and call it
public class NorthLandmark extends AppCompatActivity {

    // The protected access modifier allows all subclasses and the package to access it
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_north_landmark);
        addbuttonClickListener();
    }

    // The private access modifier prevents confusion between other declared buttons that may have the same variable name.
    private void addbuttonClickListener() {
        Button launchWS = (Button) findViewById(R.id.launchWS);
        launchWS.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

// A Uri object has been created.
// With the click of the button, the url, and hence the location of the landmark on Google Maps is opened.
        Intent intentWS = new Intent(Intent.ACTION_VIEW, Uri.parse("https://www.google.co.uk/maps/place/Camden+Passage,+London+N1/@51.5349875,-0.1738051,12z/data=!4m2!3m1!1s0x48761b5d81dbeaa1:0x82c2ba3e2cb1ed3d"));
        startActivity(intentWS);
            }

        });
    }}