package com.example.jeromehilario.nirenpatelandjeromehilariosapp;

// It was important to import the 'Uri' resource from the android net library for the correct functioning of the button.
import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

// The public class access modifier allows other objects to access and call it
public class SouthLandmark extends AppCompatActivity {

    // The protected access modifier allows all subclasses and the package to access it
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_south_landmark);

        addbuttonClickListener();
    }

    // The private access modifier prevents confusion between other declared buttons that may have the same variable name.
    private void addbuttonClickListener() {
        Button launchOA = (Button) findViewById(R.id.launchOA);
        launchOA.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

// A Uri object has been created.
// With the click of the button, the url, and hence the location of the landmark on Google Maps is opened.
                Intent intentOA = new Intent(Intent.ACTION_VIEW, Uri.parse("https://www.google.co.uk/maps/place/The+Kia+Oval/@51.4837523,-0.1169317,17z/data=!3m1!4b1!4m2!3m1!1s0x4876049284a69f57:0xd5112b823b377298"));
                startActivity(intentOA);
            }

        });
    }}